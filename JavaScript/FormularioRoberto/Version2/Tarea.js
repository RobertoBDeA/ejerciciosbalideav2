class Tarea {
    constructor(nombre,prioridad){
        this._nombre = nombre;
        this._prioridad = prioridad;
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(nombre){
        this._nombre = nombre;
    }

    get prioridad(){
        return this._prioridad;
    }

    set prioridad(prioridad){
        this._prioridad = prioridad;
    }
}