<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>Resultado</title>
            </head>
            <body>
                <ol>
                    <!--Recorremos los autores-->
                    <xsl:for-each select="catalogo/libro/autores/autor">
                        <!--Y si nacieron despues de 1900..."-->
                        <xsl:if test="@nacimiento > 1900">
                            <li>
                                <!--Entonces "retrocedemos"
                para extraer el titulo-->
                                <xsl:value-of select="../../titulo" />
                            </li>
                        </xsl:if>
                    </xsl:for-each>
                </ol>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>