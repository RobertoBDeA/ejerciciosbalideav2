<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <datos>
            <cuentas>
                <xsl:for-each select="listado/cuenta">
                    <cuenta>
                        <xsl:attribute name="dnititular">
                            <xsl:value-of select="titular/@dni" />
                        </xsl:attribute>
                        <!--Creamos el elemento "creación" que en
                    realidad contiene el texto del elemento
                    "fechacreación" original-->
                        <creacion>
                            <xsl:value-of select="fechacreacion" />
                        </creacion>
                        <!--Creamos el elemento titular y metemos
                    dentro del valor original del titular-->
                        <titular>
                            <xsl:value-of select="titular" />
                        </titular>
                        <!--Creamos el saldo actual-->
                        <saldoactual>
                            <!--Y metemos dentro la cantidad que tuviese
                        el fichero original...-->
                            <xsl:value-of select="saldoactual" />
                            <!--Y extraemos la moneda...-->
                            <xsl:value-of select="saldoactual/@moneda" />
                        </saldoactual>
                    </cuenta>
                </xsl:for-each>
            </cuentas>
            <fondos>
                <xsl:for-each select="listado/fondo">
                    <!--Paso 1: crear un fondo por cada fondo original-->
                    <fondo>
                        <!--Paso 2, crear el atributo cuentaasociada-->
                        <xsl:attribute name="cuentaasociada">
                            <xsl:value-of select="cuentaasociada" />
                        </xsl:attribute>
                        <!--Paso 3, crear el elemento cantidaddepositada-->
                        <cantidaddepositada>
                            <xsl:value-of select="datos/cantidaddepositada" />
                        </cantidaddepositada>
                        <!--Paso 4:Crear el elemento moneda-->
                        <moneda>
                            <xsl:value-of select="datos/moneda" />
                        </moneda>
                    </fondo>
                </xsl:for-each>
            </fondos>
        </datos>
    </xsl:template>
</xsl:stylesheet>