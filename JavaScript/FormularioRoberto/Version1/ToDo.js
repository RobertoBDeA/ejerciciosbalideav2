//Toma la referencia del id='formulario' del contenedor
const miFormulario = document.getElementById('formulario');
//Creamos un array vacio para guardar las tareas
const conjuntoTareas = [];

const render = () =>{
    //Toma la referencia del id='lista' del contenedor
    const conjuntoTareasFormateadas = document.getElementById('lista');
    //Asignamos una cadena vacia a conjuntoTareasFormateadas
    conjuntoTareasFormateadas.innerHTML='';
    //Recorremos el array y añadimos cada tarea a conjuntoTareasFormateadas 
    //que se mostrará en formato lista desordenada
    conjuntoTareas.forEach( tarea => conjuntoTareasFormateadas.innerHTML+=`<li>${tarea}</li>`);
    //Toma la referencia del id='lista' de la lista desordenada del contenedor
    const elementosLista = document.querySelectorAll("#lista li");
    console.log(elementosLista.value);
    //Para cada elemento de elementosLista se le añade un evento onclick
    //que lo eliminará tanto del doc html como del array
    elementosLista.forEach ((elemento,i)=>{
        elemento.addEventListener('click',()=>{
            elemento.parentNode.removeChild(elemento)
            conjuntoTareas.splice(i,1);
            render();
        })       
    })
}

miFormulario.onsubmit = (event) =>{
    event.preventDefault();
    //Toma la referencia del id='tarea' del contenedor
    const tarea = document.getElementById('tarea');
    const opcion = document.getElementById('opciones');
    console.log(opcion.value);
    //Añadimos el valor del elemento al array
    conjuntoTareas.push(tarea.value);
    //Dejamos vacio tarea
    tarea.value = '';
    render();
}