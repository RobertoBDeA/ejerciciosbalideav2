<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>Resultado</title>
            </head>
            <body>
                <table border='1'>
                    <tr>
                        <td>Nombre</td>
                        <td>Año nacimiento</td>
                    </tr>
                    <xsl:for-each select="catalogo/libro/autores/autor">
                        <xsl:if test="@nacimiento &gt; 1700">
                            <tr>
                                <td>
                                    <xsl:value-of select="." />
                                </td>
                                <td>
                                    <xsl:value-of select="@nacimiento" />
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>