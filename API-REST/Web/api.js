const express = require('express');
const Usuario = require('./usuario-controller');
const mongoose = require('mongoose');
require('dotenv').config();


const app = express();
const port = process.env.PORT;


app.use(express.json());
app.use(express.static('client'));
app.get('/', (req,res)=>{res.sendFile(`${__dirname}/index.html`)});

app.get('/usuarios/', Usuario.listarTodos)

app.post('/usuarios/', Usuario.crear)

app.get('/usuarios/:id/', Usuario.listar)

app.put('/usuarios/:id/', Usuario.actualizar)

app.patch('/usuarios/:id/', Usuario.actualizar)

app.delete('/usuarios/:id/', Usuario.borrar)

app.get('*', Usuario.noEncontrado)

// mongodb connection
mongoose
    .connect(process.env.MONGODB_URI)
    .then(() => console.log('Connection to MongoDB Atlas'))
    .catch((error) => console.error(error));

app.listen(port, ()=>{console.log("Levantando app")});