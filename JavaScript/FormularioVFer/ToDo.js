const miFormulario =document.getElementById("todo-formulario");
const conjuntoTareas = [];

const render = () =>{
    const conjuntoTareasFormateadas = document.getElementById("todo-lista");
    conjuntoTareasFormateadas.innerHTML='';
    conjuntoTareas.forEach( tarea => conjuntoTareasFormateadas.innerHTML+=`<li>${tarea}</li>`);
    const elementosLista = document.querySelectorAll("#todo-lista li");
    elementosLista.forEach ((elemento,i)=>{
        elemento.addEventListener('click',()=>{
            elemento.parentNode.removeChild(elemento)
            conjuntoTareas.splice(i,1);
            render();
        })       
    })
}

miFormulario.onsubmit = (e) =>{
    e.preventDefault();
    const tarea = document.getElementById("todo-tarea");
    conjuntoTareas.push(tarea.value);
    tarea.value='';
    render();    
}