// const Usuario
const Usuarios = require('./user.model');


const Usuario = {
    crear: async (req,res) =>{
        try{
            const user = Usuarios(req.body);
            const data = await user.save();
            console.log(data);
            res.status(201).send(data);
        } catch(err){
            console.log("Error al crear el usuario -",err);
            res.sendStatus(500);
        }
    },
    listarTodos: async (req, res) => {
        try {
            const data = await Usuarios.find();
            console.log(data);
            res.status(200).send(data);
        } catch (err) {
            console.log("Error al listar usuarios -",err);
            res.sendStatus(500);
        }
    },
    listar: async (req,res) => {
        try {
            const { id } = req.params;
            const data = await Usuarios.findById(id);
            res.status(200).send(data);
        } catch(err){
            console.log("Error al listar el usuario -",err);
            res.sendStatus(500);
        }
    },
    actualizar: async (req,res) =>{
        try {
            const { id } = req.params;
            const { name, age, email } = req.body;
            const data = await Usuarios
            .updateOne({ _id: id }, { $set: { name, age, email } });
            res.status(204).send(data);
        } catch(err){
            console.log("Error al actualizar el usuario -",err);
            res.sendStatus(500);
        }
    },
    borrar: async (req,res) =>{
        try {
            const { id } = req.params;
            const data = await Usuarios.deleteOne({ _id: id });
            res.status(204).send(data);
        } catch(err){
            console.log("Error al eliminar el usuario -",err);
            res.sendStatus(500);
        }
    },
    noEncontrado: async (req,res) =>{
        res.status(404).send('Page not found')
    }
};

module.exports = Usuario;