<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes" />

  <!-- Plantilla raíz -->
  <xsl:template match="/">
    <html>
      <body>
          <xsl:apply-templates select="catalogo/cds/cd"></xsl:apply-templates>
      </body>
    </html>
  </xsl:template>


  <!-- Plantilla cd -->
  <xsl:template match="/catalogo/cds/cd">
  <table border="1">
    <tr>
      <td>
        <xsl:apply-templates select="titulo" />
      </td>
    </tr>
  </table>
  </xsl:template>

  <!-- Plantilla titulo -->
  <xsl:template match="/catalogo/cds/cd/title">
    Title: <span style="color:#ff0000">
    <xsl:value-of select="."/></span>
    <br />
  </xsl:template>

</xsl:stylesheet>