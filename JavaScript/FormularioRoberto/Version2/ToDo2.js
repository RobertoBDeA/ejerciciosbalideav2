const tareas = [];
const botonEntrada = document.getElementById('boton-agregar');

botonEntrada.onclick = (e) => {
   e.preventDefault();
   agregarTarea(); 
}

const mostrarTareas = () =>{
    //Añade las tareas a la lista
    console.log('Mostrar tareas...');
    let texto = '';
    for(let tarea of tareas){
        console.log(tarea);
        let colorTarea;
            switch (tarea.prioridad){
                case "Baja":
                    colorTarea = "verde";
                    break;
                case "Media":
                    colorTarea = "naranja";
                    break;
                case "Alta":
                    colorTarea = "roja";
                    break;
                };
        texto += `<li class="linea-tarea" id="${colorTarea}">${tarea.nombre}</li>`;
    }
    document.getElementById('lista').innerHTML = texto;

    //Borrado de elementos li de la lista
    const nodosLI = document.querySelectorAll('#lista li');
    console.log('Mostrar nodos...')
    console.log(nodosLI);
    nodosLI.forEach((elemento,i)=>{
        elemento.addEventListener('click', () => {
            elemento.parentNode.removeChild(elemento)
            tareas.splice(i,1);
            mostrarTareas();
        })
    })
    //Borrado de todos los elementos de la lista
    const cabecero = document.getElementById('cabecero');
        cabecero.addEventListener('click', () => {
            tareas.splice(0,tareas.length);
            mostrarTareas();
        })
}

const agregarTarea = () =>{
    //Recoge los datos necesarios para crear un objeto Tarea
    const formulario = document.forms['formulario'];
    const nombre = formulario['nombre'];
    const prioridad = formulario['opciones'].value;
    //Si no le pongo la condicion, me añade valores vacios.
    if (nombre.value != '' && prioridad != ''){
        const tarea = new Tarea(nombre.value, prioridad);
        tareas.push(tarea);
        //Persistencia de datos
        let jsonTarea = JSON.stringify(tareas);
        localStorage.setItem('tareas', jsonTarea);
        nombre.value = '';
        mostrarTareas();
    }
}