<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes" />

    <!-- Plantilla raíz -->
    <xsl:template match="/">
        <html>
            <body>
                <ol>
                    <xsl:apply-templates select="catalogo/artistas/artista">
                    </xsl:apply-templates>
                </ol>
            </body>
        </html>
    </xsl:template>

    <!-- Plantilla nombre -->
    <xsl:template match="/catalogo/artistas/artista">
        <xsl:for-each select=".">
            <xsl:if test="nacionalidad = 'España'">
                <li><xsl:value-of select="nombre" /></li>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>