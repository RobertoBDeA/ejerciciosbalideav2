<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>Resultado</title>
            </head>
            <body>
                <ol>
                    <xsl:for-each select="catalogo/libro/autores/autor">
                        <xsl:sort select="." order="descending" />
                        <li>
                            <xsl:value-of select="." />
                        </li>
                    </xsl:for-each>
                </ol>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>