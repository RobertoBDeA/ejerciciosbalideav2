//Saber donde se ejecuta y el path del programa
//console.log(process.argv);

const param = (p) => {
    let index = process.argv.indexOf(p);
    //console.log(index);
    return process.argv[index + 1];
}


let nombre = param('--nombre');
let edad = param('--edad');
console.log(`Tu nombre es ${nombre} y tienes ${edad} años`);