const cargarPlantillaInicial = ()=>{

    const template =`
    <h1>Usuarios</h1>
    <form id="user-form">
        <div>
            <label>Nombre</label>
            <input name="name" />
        </div>
        <div>
            <label>Edad</label>
            <input name="age" />
        </div>
        <div>
            <label>Email</label>
            <input name="email" />
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    <ul id="user-list"></ul>
   `
   const body = document.getElementsByTagName('body')[0]
   body.innerHTML = template
}


const getUsers = async () => {
    const response = await fetch('/usuarios')
    const usuariosRecuperados = await response.json()
    
    //const aplicarPlantilla = (usuario) =>{`<li>${usuario.nombre} ${usuario.apellidos} <button data-id="${usuario._id}">Eliminar</li>`}
    function aplicarPlantilla (usuario){return `<li>${usuario.name}, ${usuario.age}, ${usuario.email} <button data-id="${usuario._id}">Eliminar<button id="actualizar" actualizar-data-id="${usuario._id}"> Actualizar </li>`}

    const userList = document.getElementById('user-list')
    userList.innerHTML = usuariosRecuperados.map(usuarioRecuperado => aplicarPlantilla(usuarioRecuperado)).join('')
    // el join es para que transforme el array de strings que devuelve el map en un string enorme sin espacios 

    //TO-DO
    //Agregar el borrado a cada button de cada usuario que acabamos de meter en la lista HTML en su evento "onclick"
    /*
    -Iteramos usuariosRecuperados con forEach
            - recuperamos el nodo del button del usuario con queryselector
                        userNode = document.querySelector(`[data-id="${usuarioDelForEach}"]`)
            - le colocamos la funcion al evento onclick de cada nodo
                dentro de la función hacemos el fetch del borrado montando la ruta poniendo el id sacado de usuarioDelForEach._id y haciendo el GET correspondiente
            -Lanzar una emergente de eliminado realizado
    */
    /*Nosotros sabemos que el li tiene un button con data-id cuyo valor es el _id de la base de datos para cada usuario,
        iteramos para ponerle a cada boton el comportamiento de borrado de cada elemento concreto*/
        usuariosRecuperados.forEach(usuarioRecuperado => {
            const botonDelete = document.querySelector(`[data-id="${usuarioRecuperado._id}"]`)   // usamos querySelector porque data-id es una propiedad custom
            //console.log(botonDelete)
            botonDelete.onclick = async (e) =>{
                await fetch(`/usuarios/${usuarioRecuperado._id}` , {
                    method: 'DELETE',
                })
                botonDelete.parentNode.remove() // sube al elemento <li> y lo borra
            }
        });

        usuariosRecuperados.forEach(usuarioRecuperado => {
            const botonActualizar = document.querySelector(`[actualizar-data-id="${usuarioRecuperado._id}"]`)   // usamos querySelector porque data-id es una propiedad custom
            console.log(botonActualizar)
            botonActualizar.onclick = async (e) =>{
                await fetch(`/usuarios/${usuarioRecuperado._id}` , {
                    method: 'PUT',
                })
                botonActualizar.parentNode.replaceChild() // sube al elemento <li> y lo borra
            }
        });
    
}


const addFormListener = async ()=> {
    const userForm = document.getElementById('user-form')
    userForm.onsubmit = async (e) =>{
        e.preventDefault()
        const formData = new FormData(userForm)
        const data = Object.fromEntries(formData.entries())
        console.log(data)
        await fetch('/usuarios', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json'
            }
        })
        getUsers()
    }

}


window.onload = () => {
    cargarPlantillaInicial()
    addFormListener()
    getUsers()
}