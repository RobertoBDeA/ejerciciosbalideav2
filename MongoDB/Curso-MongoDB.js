// Crear un Documento
// Documentos -> Objetos de tipo JSON

user1 = {
    'username': 'user1',
    'age': 27,
    'email': 'user1@example.com'
};

// Se valida que la base de datos exista.
// Se valida que la colección exista.
user2 = {
    'username':'user2',
    'age':29,
    'email':'user2@example.com'
};

user3 = {
    'username':'user3',
    'age':21,
    'email':'user3@example.com'
};

db.users.insert(user3);

// ObjectId->4
// 1.- (Timestamp)
// 2.- (Identificador para el servidor)
// 3.- (PID)
// 4.- (AutoIncrement)

// Lo métodos que se deben utilizar para la inserccion de datos son:
// insertOne() y insertMany()

// insertOne()

user4={
    'username':'user4',
    'age':31,
    'email':'user4@example.com'
};

db.users.insertOne(user4);

// insertMany()

db.users.insertMany([
    {
        'username':'user5',
        'age':43,
        'email':'user5@example.com'
    },
    {
        'username':'user6',
        'age':19,
        'email':'user6@example.com'
    },
    {
        'username':'user7',
        'age':37,
        'email':'user7@example.com'
    }
]);

// save()

user8={
    'username':'user8',
    'age':52,
    'email':'user8@example.com'
};

db.users.save(user8);


// Insertamos varios elementos más para realizar ejercicios

db.users.insertMany([
    {
        'username':'user9',
        'email':'user9@example.com',
        'age':23,
        'status':'inactive'
    },
    {
        'username':'user10',
        'email':'user10@example.com',
        'age':31,
        'status':'inactive'
    },
    {
        'username':'user11',
        'email':'user11@example.com',
        'age':8,
        'status':'inactive'
    },
    {
        'username':'user12',
        'email':'user12@example.com',
        'age':15,
        'status':'active'
    }
]);

// Obtener todos los usuarios con un edad mayor a 10
// gt : >  // gte : >= // lt : < // lte : <= // ne : !=
db.users.find({
    age:{$gt:10}
});

// Obtener la cantidad de usuarios con una edad menor a 50
db.users.find({
    age:{$lt:50}
});

// Obtener todos los usuarios con un edad mayor a 10 y cuyo estatus sea activo
db.users.find({
    $and:[
        { age:{$gt:10} },
        { status:'active' }
    ]
});

// Obtener todos los usuarios cuya edad no sea 11
db.users.find({
    age:{ $ne:11 }
});

// Obtener todos los usuarios que tengan por edad:27 o 40 o 11
db.users.find({
    $or:[
        { age:27 },
        { age:40 },
        { age:11 }
    ]
});

// 2ª Opcion $in
db.users.find({
    age: { $in: [27, 40, 11] } 
});

// Obtener todos los usuarios con atributo estatus
db.users.find({
    status: {$exists: true}
});

// Obtener todos los usuarios con estatus activo
db.users.find({
    status: 'active'
});

//2ª Opcion 
db.users.find({
    $and: [
        { status: { $exists: true}},
        { status: 'active'}
    ]
});

// Obtener todos los usuarios con estatus inactivo y correo electronico
db.users.find({
    $and: [
        { status: { $exists: true}},
        { status: 'inactive'},
        { email: { $exists: true}}
    ]
});

// Obtener el usuario con mayor edad
// age: -1 --> Ordena la edad de mayor a menor
// limit() --> limita el numero de salidas por consola
db.users.find().sort(
    {
        age: -1
    }
).limit(1);

// Obteneralos tres usuarios más jovenes
db.users.find().sort(
    {
        age: 1
    }
).limit(3);

// Emails que empiecen por user1
db.users.find( 
    {
        email: /^user1/
    } 
);

// Emails que acaben por .com
db.users.find( 
    {
        email: /.com$/
    } 
);

// Emails que contengan un 7
db.users.find( 
    {
        email: /7/
    } 
);

// Devuelve el numero de mails. Podemos poner condiciones en find.
db.users.find().count();

// Nos permites saltar los documentos que indiquemos
db.users.find().skip(5);

// Nos permite ordenar
db.users.find().sort( {age : -1} );

// Pretty nos devuelve los documentos en formato json
db.users.find().sort( {age : -1} ).skip(2).limit(3).pretty();